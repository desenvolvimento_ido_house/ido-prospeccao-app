function ControllerEndereco() {
	
	var businessDelegateEstado = new BusinessDelegateEstado();
	var businessDelegateCidade = new BusinessDelegateCidade();
	var businessDelegateBairro = new BusinessDelegateBairro();
	var businessDelegateLogradouro = new BusinessDelegateLogradouro();
	
	this.configurar = function() {
		
		businessDelegateEstado.iniciar();
		businessDelegateCidade.iniciar();
		businessDelegateBairro.iniciar();
		businessDelegateLogradouro.iniciar();
 		
	}		

}