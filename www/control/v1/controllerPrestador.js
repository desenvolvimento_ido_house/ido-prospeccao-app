
function ControllerPrestador() {

	var businessPrestador = new BusinessDelegatePrestador();
	var businessEnderecoAtendimentoPrestador = new BusinessDelegateEnderecoAtendimentoPrestador();
	var businessDelegateEnderecoAtendimentoBairroPrestador = new BusinessDelegateEnderecoAtendimentoBairroPrestador();
	var businessDelegateServico = new BusinessDelegateServico();
   	var controllerUsuario = new ControllerUsuario();
   	var controllerServico = new ControllerServico();
 	
	this.configurar = function() {
		
		//configurar controller
 		controllerUsuario.configurar();
 		
		//iniciar business
 		businessPrestador.iniciar();
 		businessEnderecoAtendimentoPrestador.iniciar();
 		businessDelegateEnderecoAtendimentoBairroPrestador.iniciar();
 		businessDelegateServico.iniciar();
   
		mudarCPFCNPJ();
 		
		//ao carregar tela
  		$(document).on('pagebeforeshow','#home', this.listarAll );	
 		$(document).on('pageshow','#home', this.imprimirUsuarios);
  		$(document).on('pageshow','#pgAdicionarPrestador', this.configurarPageCadastroPrestadorAoCarregar);
  		
 		//comandos dos botoes
   		$(document).on('vclick','#btSalvarNovoPrestador', this.salvar ); 
   		$(document).on('vclick','#prestador-list li a', this.abrirPrestadorBuscar);
  		$(document).on('vclick','#btExcluirPrestador', this.excluir);   		
   		
  		$(document).on('vclick', '#ancoraInserirNovoCliente', function() {
  			
 			var objPrestador = businessPrestador.getPrestador();
			var objUsuario = objPrestador.getUsuario();
			
			objPrestador.setId(null);
			objUsuario.setId(null);
			controllerUsuario.getBusinessDelegateUsuario().getUsuario().setId(null);
			limparFormulario();
			
  		});
  		
   		$(document).on('vclick','#btNovoPrestadorVoltar', function() {
			$.mobile.changePage('#home', {transition: AppInfo.transicao, changeHash: false}); 				
   		});
   		
	}
	
	this.listarAll = function() {
  		controllerUsuario.getBusinessDelegateUsuario().listarAll();
  		controllerServico.getBusinessDelegateServico().listarAll();
  	}
	
	this.salvar = function() {  
		
		var cnpjCpf = getCnpjCpf();
		var dataNascimento = getDataNascimento();
		var email = getEmail();
		var emailConfirmacao = getEmailConfirmacao();
		var nome = getNome();
		var senha = getSenha();
 		var tipoPessoa = getTipoPessoa();
		var geoLatitude = getGeoLatitude();
 		var geoLongitude = getGeoLongitude();
 		
 		var cep = getCep();
 		var estado = getEstado();
 		var municipio = getMunicipio();
 		var bairro = getBairro();
 		var logradouro = getLogradouro();
 		var numero = getNumero();
 		var complemento = getComplemento();
  		
		if (businessPrestador.getPrestador().getId() != null) {
			
	   		controllerUsuario.getBusinessDelegateEmailUsuario().getEmailUsuario().setUsuario(businessPrestador.getPrestador().getUsuario());
	   		controllerUsuario.getBusinessDelegateEmailUsuario().excluirAll();	 
	   		
	   		controllerUsuario.getBusinessDelegateTelefoneUsuario().getTelefoneUsuario().setUsuario(businessPrestador.getPrestador().getUsuario());
	   		controllerUsuario.getBusinessDelegateTelefoneUsuario().excluirAll();
	   		
	   		businessEnderecoAtendimentoPrestador.getEnderecoAtendimentoPrestador().setPrestador(businessPrestador.getPrestador());
	   		businessEnderecoAtendimentoPrestador.excluirAll();
	   		
	   		businessDelegateEnderecoAtendimentoBairroPrestador.getEnderecoAtendimentoBairroPrestador().setPrestador(businessPrestador.getPrestador());
	   		businessDelegateEnderecoAtendimentoBairroPrestador.excluirAll();
	   		
	   		businessPrestador.getBusinessServicoPrestador().getServicoPrestador().setPrestador(businessPrestador.getPrestador());
	   		businessPrestador.getBusinessServicoPrestador().excluirAll();
		}
		
		//salvar usuario
		var objUsuario = controllerUsuario.getBusinessDelegateUsuario().getUsuario();
		objUsuario.setId(businessPrestador.getPrestador().getUsuario().getId());
 		objUsuario.setEmail(email);
 		objUsuario.setEmailConfirmacao(emailConfirmacao);
		objUsuario.setNome(nome);
		objUsuario.setSenha(senha);
		objUsuario.setCep(cep);
		objUsuario.setEstado(estado);
		objUsuario.setMunicipio(municipio);
		objUsuario.setBairro(bairro);
		objUsuario.setLogradouro(logradouro);
		objUsuario.setNumero(numero);
		objUsuario.setComplemento(complemento);
  		var retorno = controllerUsuario.getBusinessDelegateUsuario().salvar();
 			
  		if (!retorno ) {//TODO CUIDADO POIS AS REGRAS NÃO ESTÃO SINCRONIZADAS VALIDAÇÃO BASICA POR ENQUANTO
  			return;
  		}
  		
		//salvar prestador
 		objPrestador = businessPrestador.getPrestador();
 		objPrestador.setCnpjCpf(cnpjCpf)
		objPrestador.setTipoPessoa(beanPrestadorInfo.PF);
    		
 		//TODO VERIFICAR ESSE SAVE
 		if (tipoPessoa == 'off') {
 			objPrestador.setTipoPessoa(beanPrestadorInfo.PF);
 		} else {
 			objPrestador.setTipoPessoa(beanPrestadorInfo.PJ); 			
 		}
 		objPrestador.setDataNascimento(dataNascimento);
 		objPrestador.setUsuario(objUsuario);
				
 		retorno = businessPrestador.salvar();
		
		if (!retorno) {
			return;
		}
			
   		$('#divEmail input').each(function(){
 			var email = $(this).val();
  			if (email != '' ) {
  			 	var controller = new ControllerUsuario();
   			 	controller.getBusinessDelegateEmailUsuario().getEmailUsuario();
  			 	controller.getBusinessDelegateEmailUsuario().getEmailUsuario().setUsuario(objUsuario);
  			 	controller.getBusinessDelegateEmailUsuario().getEmailUsuario().setEmail(email);
  			 	controller.getBusinessDelegateEmailUsuario().salvar();
  			 }
 		});
  		
 		$('#divTelefones input').each(function(){
 			var telefone = $(this).val();
  			if (telefone != '' ) {
  			 	var controller = new ControllerUsuario();
  			 	controller.getBusinessDelegateTelefoneUsuario().getTelefoneUsuario().setUsuario(objUsuario);
  			 	controller.getBusinessDelegateTelefoneUsuario().getTelefoneUsuario().setTelefone(telefone);
  			 	controller.getBusinessDelegateTelefoneUsuario().salvar();
  			 }
 		});
 		
 		$('#divServicos input').each(function(){
 			var servico = $(this).val();
  			if (servico != '' ) {
  			 	var businessDelegateServicoPrestador = new BusinessDelegateServicoPrestador();
  			 	businessDelegateServicoPrestador.getServicoPrestador().setDescricao(servico)
  			 	businessDelegateServicoPrestador.getServicoPrestador().setPrestador(objPrestador);
  			 	businessDelegateServicoPrestador.salvar();
  			 	
  				businessDelegateServico.getServico().setId(null);
   				businessDelegateServico.getServico().setServico(servico);
  				businessDelegateServico.getServico().setDetalhe('servico cadastrado pelo prestador ' + objUsuario.getNome());
  				businessDelegateServico.salvar();
  			 }
 		});
 	     		
  		$.each(nomeEnderecos, function(i, row) {
 			
 			businessDelegateEnderecoUsuario = new BusinessDelegateEnderecoUsuario();
 			businessDelegateEnderecoUsuario.getEnderecoUsuario().setUsuario(objUsuario)
 			
	  		$('#' + row + ' input').each(function() {
	  			
	   			var input = $(this);
	  			var tagName = input.attr('name');
	  			var valor = input.val();
	  			
	  			if ( (tagName.indexOf("cep") > -1)) {
  	  				businessDelegateEnderecoUsuario.getEnderecoUsuario().setCep(valor);
	  			}
	  			
	  			if ( (tagName.indexOf("estado") > -1)) {
	  				businessDelegateEnderecoUsuario.getEnderecoUsuario().setEstado(valor);
	  			}

	  			if ( (tagName.indexOf("municipio") > -1)) {
	  				businessEnderecoAtendimentoPrestador.getEnderecoAtendimentoPrestador().setMunicipio(valor);
	  				businessDelegateEnderecoUsuario.getEnderecoUsuario().setMunicipio(valor);
	  			}
	  			
	  			if ( (tagName.indexOf("bairro") > -1)) {
	  				businessDelegateEnderecoUsuario.getEnderecoUsuario().setBairro(valor);
	  			}
	  			
	  			if ( (tagName.indexOf("logradouro") > -1)) {
	  				businessDelegateEnderecoUsuario.getEnderecoUsuario().setLogradouro(valor);
	  			}
	  			
	  			if ( (tagName.indexOf("numero") > -1)) {
	  				businessDelegateEnderecoUsuario.getEnderecoUsuario().setNumero(valor);
	  			}
	  			
	  			if ( (tagName.indexOf("complemento") > -1)) {
	  				businessDelegateEnderecoUsuario.getEnderecoUsuario().setComplemento(valor);
	  			}
	  		});
	  		retorno = businessDelegateEnderecoUsuario.salvar();
  		});

  		nomebairrosAtendimento.push('enderecos');
  		$.each(nomebairrosAtendimento, function(i, row) {
 			
 			businessEnderecoAtendimentoPrestador = new BusinessDelegateEnderecoAtendimentoPrestador();
 			businessEnderecoAtendimentoPrestador.getEnderecoAtendimentoPrestador().setPrestador(businessPrestador.getPrestador());
	 		var bairros = [];
	 		
	  		$('#' + row + ' input').each(function() {
	  			
	   			var input = $(this);
	  			var tagName = input.attr('name');
	  			var valor = input.val();
	  			
	  			if ( (tagName.indexOf("cepAtendimento") > -1)) {
 	  				businessEnderecoAtendimentoPrestador.getEnderecoAtendimentoPrestador().setCep(valor);
	  			}
	  			
	  			if ( (tagName.indexOf("estadoAtendimento") > -1)) {
	  				businessEnderecoAtendimentoPrestador.getEnderecoAtendimentoPrestador().setEstado(valor);
	  			}

	  			if ( (tagName.indexOf("municipioAtendimento") > -1)) {
	  				businessEnderecoAtendimentoPrestador.getEnderecoAtendimentoPrestador().setMunicipio(valor);
	  			}
	  			
	  			if (tagName.indexOf("bairro") > -1) {
	   				bairros.push(valor); 	
	  			}
	  			
	 		});
	  		
	  		retorno = businessEnderecoAtendimentoPrestador.salvar();
  		    $.each(bairros, function(i, row) {
		    	var businessAtendimentoBairroPrestador = new BusinessDelegateEnderecoAtendimentoBairroPrestador();
		    	businessAtendimentoBairroPrestador.getEnderecoAtendimentoBairroPrestador().setPrestador(businessPrestador.getPrestador());
	 	    	businessAtendimentoBairroPrestador.getEnderecoAtendimentoBairroPrestador().setEnderecoAtendimentoPrestador(businessEnderecoAtendimentoPrestador.getEnderecoAtendimentoPrestador());
	 	    	businessAtendimentoBairroPrestador.getEnderecoAtendimentoBairroPrestador().setBairro(row);
	 	    	businessAtendimentoBairroPrestador.salvar();
		    });  		
 		});
  		
		if (retorno) {
			$.mobile.changePage('#home', {transition: AppInfo.transicao, changeHash: false}); 				
		}
 	}
	
	this.configurarPageCadastroPrestadorAoCarregar = function() {
			
		if (businessPrestador.getPrestador().getId() == null) {
						
			autoCompletarServico();

			
			$('#liConfirmacaoEmail').show();	
	        $('#btExcluirPrestador').attr('disabled', 'disabled');
	        $('#divTituloPrestador').html('Novo Prestador');
	        $('#btSalvarNovoPrestador').html('Salvar');
	        
			var objPrestador = businessPrestador.getPrestador();
			var objUsuario = objPrestador.getUsuario();
			
			//limpar objeto Prestado
			objPrestador.setId(null);
			objPrestador.setCnpjCpf(null);
			objPrestador.setDataNascimento(null);
			objPrestador.setTipoPessoa(null);
 			
			//limpar objeto usuario
			objUsuario.setId(null);
			objUsuario.setEmail(null);
			objUsuario.setNome(null);
			objUsuario.setSenha(null);
			
	 		$('#divTelefones div').each(function(){
	 			 $(this).remove();
	 		});
	 		
	 		$('#divEmail div').each(function(){
				$(this).remove();
			});
	 		
	 		$('#divTelefones input').each(function(){
	 			 $(this).remove();
	  			adicionarCampo('divTelefones','telefone_1' ,'', 'telefone');
	 		});
	 		
	 		$('#divEmail input').each(function(){
				$(this).remove();
	 			adicionarCampo('divEmail','email_1' , '', 'email');
			});
	 		
	 		$('#divServicos input').each(function(){
	 			 //$(this).remove();
	  			//adicionarCampo('divServicos','servico_1' , '', 'text');
	 		});
	 		
	 		
	 		$.each(nomeEnderecos, function(i, row) {
 	 			$('#' + row).remove();
 	 		});
	 		
	 		$.each(nomebairrosAtendimento, function(i, row) {
 	 			$('#' + row).remove();
 	 		});
	 	
 		} else {
			abrirPrestador();
			$('#liConfirmacaoEmail').hide();
	        $('button').removeAttr('disabled');
	        $('#divTituloPrestador').html('Editar Prestador');
	        $('#btSalvarNovoPrestador').html('Editar');
 		}
	}
	    
	this.abrirPrestadorBuscar = function() {
		limparFormulario();
		businessPrestador.getPrestador().getUsuario().setId($(this).attr('data-id'));
  		businessPrestador.buscar();
 	    $.mobile.changePage( "#pgAdicionarPrestador", { transition: AppInfo.transicao, changeHash: false });
	}
	
	function abrirPrestador(){
				 		
		var objPrestador = businessPrestador.getPrestador();
		var objUsuario = objPrestador.getUsuario();
		
		//dados gerais
 	   	$('#nome').val(objUsuario.getNome());
		$('#email').val(objUsuario.getEmail());
		$('#senha').val(objUsuario.getSenha());
		
		//endereco principal
		$('#cep').val(objUsuario.getCep());
		$('#estado').val(objUsuario.getEstado());
		$('#municipio').val(objUsuario.getMunicipio());
		$('#bairro').val(objUsuario.getBairro());
		$('#logradouro').val(objUsuario.getLogradouro());
		$('#numero').val(objUsuario.getNumero());
		$('#complemento').val(objUsuario.getComplemento());
 		
		//dados do prestador
		$('#cnpjCpf').val(objPrestador.getCnpjCpf());
		$('#dataNascimento').val(trim(objPrestador.getDataNascimento()));
		
		//configuracao do tipo de pessoa
		if ( trim(objPrestador.getTipoPessoa()) == beanPrestadorInfo.PJ ) {
			var tipoPessoa = $('#tipoPessoa');
			tipoPessoa[0].selectedIndex = 1;
			tipoPessoa.slider('refresh');
		} else if (trim(objPrestador.getTipoPessoa()) == beanPrestadorInfo.PF) {
			var tipoPessoa = $('#tipoPessoa');
			tipoPessoa[0].selectedIndex = 0;
			tipoPessoa.slider('refresh');
		}
 				
		//lista de email e telefone
 		$.each(businessPrestador.getPrestador().getUsuario().getEmails() , function(i, row) {
 			adicionarCampo('divEmail','email_' + i , row.getEmail(), 'email');
		});
		
 		$.each(businessPrestador.getPrestador().getUsuario().getTelefones() , function(i, row) {
 			adicionarCampo('divTelefones','telefone_' + i , row.getTelefone(), 'telefone');
		});
 		
 		$.each(businessPrestador.getPrestador().getServicos() , function(i, row) {
 			adicionarCampo('divServicos','servico_' + i , row.getDescricao());
		});
 		
 		$.each(businessPrestador.getPrestador().getUsuario().getEnderecos() , function(i, row) {
  			adicionarEndereco();
		});
 		
 		$.each(businessPrestador.getPrestador().getEnderecosAtendimento() , function(i, row) {
 			adicionarEnderecoAtendimento();
 		});
  	
   	}
	
	function autoCompletarServico() {
		
		var availableTags = [];
		
 		$.each(controllerServico.getMemoryRows() , function(i, row) {
 			availableTags.push(row.servico); 	
  		});
 		
 		$('#divServicos input').each(function(){
  			var input = $(this);
  			var tagName = input.attr('name');
  			
 			$( '#' + tagName ).bind( "keydown", function( event ) {
 		        if ( event.keyCode === $.ui.keyCode.TAB &&
 		            $( this ).autocomplete( "instance" ).menu.active ) {
 		          event.preventDefault();
 		        }
 		      }).autocomplete({
 	 	        minLength: 0,
 		        source: function( request, response ) {
 	 	          response( $.ui.autocomplete.filter(availableTags, extractLast( request.term ) ) );
 		        },
 		        focus: function() {
 	 	          return false;
 		        },
 		        select: function( event, ui ) {
 		          var terms = split( this.value );
 	 	          terms.pop();
 	 	          terms.push( ui.item.value );
 	 	          terms.push("");
 		          this.value = terms.join( ", " );
 		          return false;
 		        }
 		      });
 		});
	}
	
	function extractLast( term ) {
		return split( term ).pop();
	}
		
	this.excluir = function() {
	
   		controllerUsuario.getBusinessDelegateEmailUsuario().getEmailUsuario().setUsuario(businessPrestador.getPrestador().getUsuario());
   		controllerUsuario.getBusinessDelegateEmailUsuario().excluirAll();
   		   		
   		controllerUsuario.getBusinessDelegateTelefoneUsuario().getTelefoneUsuario().setUsuario(businessPrestador.getPrestador().getUsuario());
   		controllerUsuario.getBusinessDelegateTelefoneUsuario().excluirAll();
   		
   		businessEnderecoAtendimentoPrestador.getEnderecoAtendimentoPrestador().setPrestador(businessPrestador.getPrestador());
   		businessEnderecoAtendimentoPrestador.excluirAll();
   		
   		businessDelegateEnderecoAtendimentoBairroPrestador.getEnderecoAtendimentoBairroPrestador().setPrestador(businessPrestador.getPrestador());
   		businessDelegateEnderecoAtendimentoBairroPrestador.excluirAll();
   		
   		businessPrestador.getBusinessServicoPrestador().getServicoPrestador().setPrestador(businessPrestador.getPrestador());
   		businessPrestador.getBusinessServicoPrestador().excluirAll();
   		
		businessPrestador.excluir();
		controllerUsuario.excluir();

 		$.mobile.changePage('#home', {transition: AppInfo.transicao, changeHash: false}); 				
 	}
	
	this.imprimirUsuarios = function() {
		
		$('#prestador-list').empty();
	    $.each(controllerUsuario.getBusinessDelegateUsuario().getMemoryRows(), function(i, row) {
	    	$('#prestador-list').append('<li><a href="" data-id="' + row.id + '"><h3>' + row.nome + '</h3><p>' + row.email + '</p></a></li>');
	    });
	    $('#prestador-list').listview('refresh');
	}
	
	function mudarCPFCNPJ(){
		$("input[name='radioTipoPessoa']").bind( "change", "#", function() {
	        if ($("#radioFisica").is(":checked")) {
				$('#cpfoucnpj').html ('CPF:');
				$('#liDataNascimento').css('display','');
	        }
	        else if ($("#radioJuridica").is(":checked") ){
				$('#cpfoucnpj').html ('CNPJ:');
				$('#liDataNascimento').css('display','none');
	        }
	    });
	}

	function limparFormulario() {
		
		//limpar usuario e prestador
 		$('#nome').val('');
		$('#dataNascimento').val('');
		$('#email').val('');
		$('#cnpjCpf').val('');
		$('#senha').val('');
		$('#tipoPessoa').val('');
		$('#confirmarEmail').val('');
		
		//limpar endereco principal
		$('#cep').val('');
		$('#estado').val('');
		$('#municipio').val('');
		$('#bairro').val('');
		$('#logradouro').val('');
		$('#numero').val('');
		$('#complemento').val('');
		
		var emails = businessPrestador.getPrestador().getUsuario().getEmails();
		var telefones = businessPrestador.getPrestador().getUsuario().getTelefones();
		var servicos = businessPrestador.getPrestador().getServicos();
		var enderecosUsuario = businessPrestador.getPrestador().getUsuario().getEnderecos();
		var enderecoAtendimentoPrestador = businessPrestador.getPrestador().getEnderecosAtendimento();
 		
		emails.length = 0;
		telefones.length = 0;
		servicos.length = 0;
		enderecosUsuario.length = 0;
		enderecoAtendimentoPrestador.length = 0;
		
        $('#btExcluirPrestador').attr('disabled', 'disabled');
        
 		$('#divTelefones div').each(function(){
 			 $(this).remove();
 		});
 		
 		$('#divEmail div').each(function(){
			$(this).remove();
		});
		
		$('#divServicos div').each(function(){
			//$(this).remove();
		});
		
		$('#divTelefones input').each(function(){
			 $(this).remove();
 			adicionarCampo('divTelefones','telefone_1' ,'', 'telefone');
		});
		
		$('#divEmail input').each(function(){
			$(this).remove();
			adicionarCampo('divEmail','email_1' , '', 'email');
		});
		
		$('#divServicos input').each(function(){
			 //$(this).remove();
 			//adicionarCampo('divServicos','servico_1' , '', 'text');
		});
		
		$.each(nomeEnderecos, function(i, row) {
			$('#' + row).remove();
	 	});
 		
 		$.each(nomebairrosAtendimento, function(i, row) {
 			$('#' + row).remove();
	 	});
 		
 		nomeEnderecos.length = 0;
 		nomebairrosAtendimento.length = 0;
 		
	}
	
	function getNome() {
		 return $('#nome').val();
	}
	
	function getDataNascimento() {
		 return $('#dataNascimento').val();
	} 
	
	function getEmail() {
		 return $('#email').val();
	}
	
	function getEmailConfirmacao() {
		return $('#emailConfirmacao').val();
	}
	function getCnpjCpf() {
		 return $('#cnpjCpf').val();
	}
	
	function getSenha() {
		 return $('#senha').val();
	}
		
	function getTipoPessoa() {		
		 return $('#tipoPessoa').val();
	}	
	
	function getCep() {
		 return $('#cep').val();
	}
	
	function getEstado() {
		 return $('#estado').val();
	}
	
	function getMunicipio() {
		 return $('#municipio').val();
	}
	
	function getBairro() {
		 return $('#bairro').val();
	}
	
	function getLogradouro() {
		 return $('#logradouro').val();
	}

	function getNumero() {
		return $('#numero').val();
	}
	
	function getComplemento() {
		return $('#complemento').val();
	}
	
	function getGeoLatitude() {
		return getLatitude();
	}
	
	function getGeoLongitude() {
		return getLongitude();
	}
	 
}