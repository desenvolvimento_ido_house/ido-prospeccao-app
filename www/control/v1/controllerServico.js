var controllerServicoInfo = {
		
}

function ControllerServico() {
		
 	var businessDelegateServico = new BusinessDelegateServico();
 
 	this.getBusinessDelegateServico = function() {
 		return businessDelegateServico;
 	}
 	
	this.configurar = function() {
		
		//iniciar table
		businessDelegateServico.iniciar();
		
		//ao carregaroTelas
		$(document).on('pagebeforeshow','#lstServico', this.listarAll );	
 		$(document).on('pageshow','#lstServico', this.printServicos);
 		
 		//comandos dos botoes
   		$(document).on('vclick','#btSalvarServico', this.salvar );
   		
 
		$(document).on('vclick','#btCancelarServico', function(){
			$.mobile.changePage('#lstServico', {transition: AppInfo.transicao, changeHash: false});
		});

   		$(document).on('vclick','#btListaServicosVoltar', function() {
			$.mobile.changePage('#home', {transition: AppInfo.transicao, changeHash: false}); 				
   		});
   		
   		$(document).on('vclick','#btNovoServicoVoltar', function() {
			$.mobile.changePage('#lstServico', {transition: AppInfo.transicao, changeHash: false});
   		});
   		
   		$(document).on('vclick','#btVoltarListaServico', function() {
			$.mobile.changePage('#lstServico', {transition: AppInfo.transicao, changeHash: false});
   		});
   		
   		$(document).on('vclick','#btNovoServico', function() {
			$.mobile.changePage('#pgAdicionarServico', {transition: AppInfo.transicao, changeHash: false});
   		});
   		
		$(document).on('vclick','#btEditarServico', this.editar );
		$(document).on('vclick','#btExcluirServico', this.excluir);
	
  		$(document).on('vclick', '#servico-list li a', this.abrirServico);
 		 		
	}
	
	this.salvar = function() {  
	 	
		var servico = getDescricao();
		var detalhe = getDetalhe();
		
		businessDelegateServico.getServico().setId(null);
		businessDelegateServico.getServico().setServico(servico);
		businessDelegateServico.getServico().setDetalhe(detalhe);
 
		var retorno = businessDelegateServico.salvar();
			
		if (retorno) {
			$.mobile.changePage('#lstServico', {transition: AppInfo.transicao, changeHash: false}); 				
		}
 	}
	
	this.editar = function() {
		
	 	var servico = $('#descricaoServicoEditar').val();
		var detalhe = $('#detalheServicoEditar').val();
		
		businessDelegateServico.getServico().setServico(servico);
		businessDelegateServico.getServico().setDetalhe(detalhe);
		var retorno = businessDelegateServico.salvar();
			
		if (retorno) {
	 	   	$.mobile.changePage('#lstServico', {transition: AppInfo.transicao, changeHash: false}); 				
		}
	}
	
	this.excluir = function() {
		businessDelegateServico.excluir();
      	$.mobile.changePage('#lstServico', {transition: AppInfo.transicao, changeHash: false});
 	}
	
	this.listarAll = function() {
		limparFormulario();
 		businessDelegateServico.listarAll();
	}
	
	this.printServicos = function() {
		$('#servico-list').empty();
		    $.each(businessDelegateServico.getMemoryRows(), function(i, row) {
		    $('#servico-list').append('<li><a href="" data-id="' + row.id + '"><h3>' + row.servico + '</h3><p>' + row.detalhe + '</p></a></li>');
	    });
	    $('#servico-list').listview('refresh');
	}
	
	this.abrirServico = function() {
		businessDelegateServico.getServico().setId($(this).attr('data-id'))
	    $.each(businessDelegateServico.getMemoryRows(), function(i, row) {
	        if(row.id == businessDelegateServico.getServico().getId()) {
	        	$('#descricaoServicoEditar').val(row.servico);
		    	$('#detalheServicoEditar').val(row.detalhe);            
	        }
	    }); 
	    $.mobile.changePage( "#pgEditarnarServico", { transition: AppInfo.transicao, changeHash: false });
	}
	
	this.getMemoryRows = function() {
		return businessDelegateServico.getMemoryRows()
	}
	
	function getDescricao() {
		 return $('#descricaoServico').val();
	}
	
	function getDetalhe() {
		return $('#detalheServico').val();
	}
	
	function limparFormulario() {
		$('#descricaoServicoEditar').val('');
		$('#detalheServicoEditar').val('');
		$('#descricaoServico').val('');
		$('#detalheServico').val('');
	}
	
}
