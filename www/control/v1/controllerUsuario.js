function ControllerUsuario() {
	
	var businessDelegateUsuario = new BusinessDelegateUsuario();	
	var businessDelegateTelefoneUsuario = new BusinessDelegateTelefoneUsuario();
 	var businessDelegateEmailUsuario = new BusinessDelegateEmailUsuario();
 	var businessDelegateEnderecoUsuario = new BusinessDelegateEnderecoUsuario();
 	
	this.getBusinessDelegateUsuario = function() {
		return businessDelegateUsuario;
	}
	
	this.getBusinessDelegateTelefoneUsuario = function() {
		return businessDelegateTelefoneUsuario;
	}
	
	this.getBusinessDelegateEmailUsuario = function() {
		return businessDelegateEmailUsuario;
	}
	
	this.getBusinessDelegateEnderecoUsuario = function() {
		return BusinessDelegateEnderecoUsuario;
	}
	
	this.configurar = function() {
		businessDelegateUsuario.iniciar();
		businessDelegateTelefoneUsuario.iniciar();
		businessDelegateEmailUsuario.iniciar();
		businessDelegateEnderecoUsuario.iniciar();
	}
	
	this.excluir = function() {
		businessDelegateUsuario.excluir();
	}
	
	this.listarAll = function() {
		businessDelegateUsuario.listarAll();
	}
	
	
}