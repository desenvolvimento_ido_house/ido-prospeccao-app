var AppInfo = {
		name: 'ido',
		version : '1.0',
		desenvolvedor: 'equipe_ido_desenvolvimento',
		transicao : 'pop',
		dropAllTableAoIniciar : false
}

function App() {
	
	var controllerServico = new ControllerServico();
	var controllerPrestador = new ControllerPrestador();
	var controllerEndereco = new ControllerEndereco();
    	
	this.iniciar = function() {
		console.log('init application ' + AppInfo.name + ' -version ' + AppInfo.version + ' -desenvolvedor ' + AppInfo.desenvolvedor);
 		this.initDatabase(); 
 		controllerServico.configurar();
 		controllerPrestador.configurar();
 		controllerEndereco.configurar();
   	}
	
	this.initDatabase = function() {
		db.database = window.openDatabase(db.nome, db.versao , db.apelido , db.maxSizeInBytes);	
	}	
}