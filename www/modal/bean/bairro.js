function Bairro(){

	var id = null;
	var cidade;
	var descricao;
	
	this.setId = function(_id) {
		this.id = _id;
	}

	this.setCidade = function(_cidade) {
		this.cidade = _cidade;
	}
	
	this.setDescricao = function(_descricao) {
		this.descricao = _descricao;
	}
	 
	this.getId = function() {
		return this.id;
	}
	
	this.getCidade = function() {
		return this.cidade;
	}
	
	this.getDescricao = function() {
		return this.descricao;
	}

}