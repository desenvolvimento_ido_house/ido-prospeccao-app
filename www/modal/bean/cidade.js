function Cidade(){

	var id = null;
	var estado;
	var descricao;
	var cepgeral;
	
	this.setId = function(_id) {
		this.id = _id;
	}

	this.setEstado = function(_estado) {
		this.estado = _estado;
	}
	
	this.setDescricao = function(_descricao) {
		this.descricao = _descricao;
	}
	
	this.setCEPGeral = function(_cepgeral) {
		this.cepgeral = _cepgeral;
	}
	 
	this.getId = function() {
		return this.id;
	}
	
	this.getEstado = function() {
		return this.estado;
	}
	
	this.getDescricao = function() {
		return this.descricao;
	}
	
	this.getCEPGeral = function() {
		return this.cepgeral;
	}

}