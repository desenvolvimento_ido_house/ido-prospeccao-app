function Estado(){

	var id = null;
	var descricao;
	var uf;
	
	this.setId = function(_id) {
		this.id = _id;
	}
	
	this.setDescricao = function(_descricao) {
		this.descricao = _descricao;
	}
	
	this.setUF = function(_uf) {
		this.uf = _uf;
	}
	 
	this.getId = function() {
		return this.id;
	}
	
	this.getDescricao = function() {
		return this.descricao;
	}
	
	this.getUF = function() {
		return this.uf;
	}

}