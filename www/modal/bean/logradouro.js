function Logradouro(){

	var id = null;
	var tipoLogradouro;
	var bairro;
	var descricao;
	var cep;
	
	this.setId = function(_id) {
		this.id = _id;
	}

	this.setTipoLogradouro = function(_tipoLogradouro) {
		this.tipoLogradouro = _tipoLogradouro;
	}
	
	this.setBairro = function(_bairro) {
		this.bairro = _bairro;
	}

	this.setDescricao = function(_descricao) {
		this.descricao = _descricao;
	}
	
	this.setCEP = function(_cep) {
		this.cep = _cep;
	}
	 
	this.getId = function() {
		return this.id;
	}
	
	this.getTipoLogradouro = function() {
		return this.tipoLogradouro;
	}
	
	this.getBairro = function() {
		return this.bairro;
	}
	
	this.getDescricao = function() {
		return this.descricao;
	}
	
	this.getCEP = function() {
		return this.cep;
	}

}