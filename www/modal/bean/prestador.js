var beanPrestadorInfo = {
		PJ : 'pj',//para pessoa juridica
		PF : 'pf'//para pessoa fisica
}

function Prestador() {
	
	var tipoPessoa;
	var cnpjCpf;
	var dataNascimento;
  	var usuario = new Usuario();

	var servicos = [];
	var enderecosAtendimento = [];
	
	this.setId = function(_id) {
		this.id = _id;
	}
	
	this.getId = function() {
		return this.id;
	}
	
	this.setTipoPessoa = function(_tipoPessoa) {
		this.tipoPessoa = _tipoPessoa;
	}
	
	this.getTipoPessoa = function() {
		return this.tipoPessoa;
	}

	this.setCnpjCpf = function(_cnpjCpf) {
		this.cnpjCpf = _cnpjCpf;
	}
	
	this.getCnpjCpf = function() {
		return this.cnpjCpf;
	}
	
	this.setDataNascimento = function(_dataNascimento) {
		dataNascimento = _dataNascimento;
	}
	
	this.getDataNascimento = function() {
		return dataNascimento;
	}
	
	this.setUsuario = function(_usuario) {
		usuario = _usuario;
	}
	
	this.getUsuario = function() {
		return usuario;
	}
	
	this.getServicos = function() {
		return servicos;
	}
	
	this.getEnderecosAtendimento = function() {
		return enderecosAtendimento;
	}
 }