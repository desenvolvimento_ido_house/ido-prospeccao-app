function Servico() {
		
		var id = null;
		var servico;
		var detalhe;
		
		this.setId = function(_id) {
			this.id = _id;
		}
		
		this.setServico = function(_servico) {
			this.servico = _servico;
		}
		
		this.setDetalhe = function(_detalhe) {
			this.detalhe = _detalhe;
		}
		 
		this.getId = function() {
			return this.id;
		}
		
		this.getServico = function() {
			return this.servico;
		}
		
		this.getDetalhe = function() {
			return this.detalhe;
		}
 }