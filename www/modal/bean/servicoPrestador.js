function ServicoPrestador() {
	
	var id = null;
	var prestador = new Prestador();
	var servico = new Servico();
	var descricao;
	
	this.setDescricao = function(_descricao) {
		this.descricao = _descricao;
	}
	
	this.getDescricao = function() {
		return this.descricao;
	}
	
	this.setId = function(_id) {
		this.id = _id;
	}
	
	this.getId = function() {
		return this.id;
	}
	
	this.setPrestador = function(_prestador) {
		this.prestador = _prestador;
	}
	
	this.getPrestador = function() {
		return this.prestador;
	}
	
	this.setServico = function(_servico) {
		this.servico = _servico;
	}
	
	this.getServico = function() {
		return this.servico;
	}
}