function TelefoneUsuario() {
	
	var id = null;
	var usuario = new Usuario();
	var telefone = '';
	var tipoTelefone = '';
	
	this.setId = function(_id) {
		this.id = _id;
	}
	
	this.getId = function() {
		return this.id;
	}
	
	this.setUsuario = function(_usuario) {
		this.usuario = _usuario;
	}
	
	this.getUsuario = function() {
		return this.usuario;
	}
	
	this.setTelefone = function(_telefone) {
		this.telefone = _telefone;
	}
	
	this.getTelefone = function() {
		return this.telefone;
	}
	
	this.setTipoTelefone = function(_tipoTelefone) {
		this.tipoTelefone = _tipoTelefone;
	}
	
	this.getTipoTelefone = function() {
		return this.tipoTelefone;
	}
	
}