function BusinessDelegateBairro() {
 	
	var bairro = new Bairro();
	var daoBairro = new DAOBairro();
   
	this.getBairro = function() {
		return bairro;
	}
	
 	this.iniciar = function() {
 		
 		if (AppInfo.dropAllTableAoIniciar) {
 			daoBairro.dropTable();
 		}
 		
	    db.database.transaction(this.criarTabela, function(err) {
	    	console.log("Error processing SQL: " + err.code);
	    }, function() {
	    	console.log("success create table " + bairroInfo.NOME_TABELA);
	    });	
	}
	
	this.criarTabela = function(tx) {
		daoBairro.createTable(tx);
	}
	
	this.salvar = function() {  
	 	
		if (this.getBairro().getCidade() == '' 
			|| this.getBairro().getCidade() == undefined) {
			alert('Cidade é obrigatório');//TODO CALLBACK PARA RETORNO COM UM OBJETO
			return false;
		}
	 	
		if (this.getBairro().getDescricao() == '' 
			|| this.getBairro().getDescricao() == undefined) {
			alert('Descrição da bairro é obrigatório');//TODO CALLBACK PARA RETORNO COM UM OBJETO
			return false;
		}
		
		if (bairro.getId() != null) {
			daoBairro.update(bairro);
		} else {
			daoBairro.salvar(bairro);			
		}
		
		return true;
	}
		
	this.excluir = function() {
		daoBairro.deletar(bairro);
 	}
	
	this.listarAll = function() {
 		daoBairro.listarAll();	
	}
	
}