function BusinessDelegateCidade() {
 	
	var cidade = new Cidade();
	var daoCidade = new DAOCidade();
   
	this.getCidade = function() {
		return cidade;
	}
	
 	this.iniciar = function() {
 		
 		if (AppInfo.dropAllTableAoIniciar) {
 			daoCidade.dropTable();
 		}
 		
	    db.database.transaction(this.criarTabela, function(err) {
	    	console.log("Error processing SQL: " + err.code);
	    }, function() {
	    	console.log("success create table " + cidadeInfo.NOME_TABELA);
	    });	
	}
	
	this.criarTabela = function(tx) {
		daoCidade.createTable(tx);
	}
	
	this.salvar = function() {  
	 	
		if (this.getCidade().getEstado() == '' 
			|| this.getCidade().getEstado() == undefined) {
			alert('Estado é obrigatório');//TODO CALLBACK PARA RETORNO COM UM OBJETO
			return false;
		}
	 	
		if (this.getCidade().getDescricao() == '' 
			|| this.getCidade().getDescricao() == undefined) {
			alert('Descrição da cidade é obrigatório');//TODO CALLBACK PARA RETORNO COM UM OBJETO
			return false;
		}
		
		if (this.getCidade().getCEPGeral() == '' 
			|| this.getCidade().getCEPGeral() == undefined) {
			alert('CEP geral da cidade é obrigatório.');
			return false;
		}
		
		if (cidade.getId() != null) {
			daoCidade.update(cidade);
		} else {
			daoCidade.salvar(cidade);			
		}
		
		return true;
	}
		
	this.excluir = function() {
		daoCidade.deletar(cidade);
 	}
	
	this.listarAll = function() {
 		daoCidade.listarAll();	
	}
	
}