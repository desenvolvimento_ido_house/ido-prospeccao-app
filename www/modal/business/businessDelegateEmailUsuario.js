function BusinessDelegateEmailUsuario() {

	var emailUsuario = new EmailUsuario();
	var daoEmailUsuario = new DAOEmailUsuario(); 

	this.getEmailUsuario = function() {
		return emailUsuario;
	}
	
	this.iniciar = function() {
		
 		if (AppInfo.dropAllTableAoIniciar) {
 			daoEmailUsuario.dropTable();
 		}
 		
	    db.database.transaction(this.criarTabela, function(err) {
	    	console.log("Error processing SQL: " + err.code);
	    }, function() {
	    	console.log("success create table " + emailUsuarioInfo.NOME_TABELA);
	    });	
	}
	
	this.criarTabela = function(tx) {
		daoEmailUsuario.createTable(tx);
	}
 	
	this.salvar = function() {  
		if (emailUsuario.getId() != null) {
			daoEmailUsuario.update(emailUsuario);
		} else {
			daoEmailUsuario.salvar(emailUsuario);			
		}
		return true;
	}
	
	this.excluir = function() {
		daoEmailUsuario.deletar(emailUsuario);
	}
	
	this.excluirAll = function() {
		daoEmailUsuario.deletarAll(emailUsuario);
	}
}