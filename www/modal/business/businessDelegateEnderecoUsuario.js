function BusinessDelegateEnderecoUsuario(){
	
	var enderecoUsuario = new EnderecoUsuario();
	var daoEnderecoUsuario = new DAOEnderecoUsuario();

	this.getEnderecoUsuario = function() {
		return enderecoUsuario;
	}
	
 	this.iniciar = function() {
 		
 		if (AppInfo.dropAllTableAoIniciar) {
 			daoEnderecoUsuario.dropTable();
 		}
 		
	    db.database.transaction(this.criarTabela, function(err) {
	    	console.log("Error processing SQL: " + err.code);
	    }, function() {
	    	console.log("success create table " + daoEnderecoUsuarioInfo.NOME_TABELA);
	    });	
	}
 	
 	this.criarTabela = function(tx) {
 		daoEnderecoUsuario.createTable(tx)
 	}
 	
 	this.salvar = function() {
 		daoEnderecoUsuario.salvar(enderecoUsuario);
 		return true;
 	}
}