function BusinessDelegateEstado() {
 	
	var estado = new Estado();
	var daoEstado = new DAOEstado();
   
	this.getEstado = function() {
		return estado;
	}
	
 	this.iniciar = function() {
 		
 		if (AppInfo.dropAllTableAoIniciar) {
 			daoEstado.dropTable();
 		}
 		
	    db.database.transaction(this.criarTabela, function(err) {
	    	console.log("Error processing SQL: " + err.code);
	    }, function() {
	    	console.log("success create table " + estadoInfo.NOME_TABELA);
	    });	
	}
	
	this.criarTabela = function(tx) {
		daoEstado.createTable(tx);
	}
	
	this.salvar = function() {  
	 	
		if (this.getEstado().getDescricao() == '' 
			|| this.getEstado().getDescricao() == undefined) {
			alert('Descrição do estado é obrigatório');//TODO CALLBACK PARA RETORNO COM UM OBJETO
			return false;
		}
		
		if (this.getEstado().getUF() == '' 
			|| this.getEstado().getUF() == undefined) {
			alert('UF do estado é obrigatório.');
			return false;
		}
		
		if (estado.getId() != null) {
			daoEstado.update(estado);
		} else {
			daoEstado.salvar(estado);			
		}
		
		return true;
	}
		
	this.excluir = function() {
		daoEstado.deletar(estado);
 	}
	
	this.listarAll = function() {
 		daoEstado.listarAll();	
	}
	
}