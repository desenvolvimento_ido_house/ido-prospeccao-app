function BusinessDelegateLogradouro() {
 	
	var logradouro = new Logradouro();
	var daoLogradouro = new DAOLogradouro();
   
	this.getLogradouro = function() {
		return logradouro;
	}
	
 	this.iniciar = function() {
 		
 		if (AppInfo.dropAllTableAoIniciar) {
 			daoLogradouro.dropTable();
 		}
 		
	    db.database.transaction(this.criarTabela, function(err) {
	    	console.log("Error processing SQL: " + err.code);
	    }, function() {
	    	console.log("success create table " + logradouroInfo.NOME_TABELA);
	    });	
	}
	
	this.criarTabela = function(tx) {
		daoLogradouro.createTable(tx);
	}
	
	this.salvar = function() {  

		if (this.getLogradouro().getTipoLogradouro() == '' 
			|| this.getLogradouro().getTipoLogradouro() == undefined) {
			alert('Tipo de logradouro é obrigatório');//TODO CALLBACK PARA RETORNO COM UM OBJETO
			return false;
		}

		if (this.getLogradouro().getBairro() == '' 
			|| this.getLogradouro().getBairro() == undefined) {
			alert('Bairro é obrigatório');//TODO CALLBACK PARA RETORNO COM UM OBJETO
			return false;
		}
		
		if (this.getLogradouro().getDescricao() == '' 
			|| this.getLogradouro().getDescricao() == undefined) {
			alert('Descrição da logradouro é obrigatório');//TODO CALLBACK PARA RETORNO COM UM OBJETO
			return false;
		}

		if (this.getLogradouro().getCEP() == '' 
			|| this.getLogradouro().getCEP() == undefined) {
			alert('CEP é obrigatório');//TODO CALLBACK PARA RETORNO COM UM OBJETO
			return false;
		}
	 	
		if (logradouro.getId() != null) {
			daoLogradouro.update(logradouro);
		} else {
			daoLogradouro.salvar(logradouro);	
		}
		
		return true;
	}
		
	this.excluir = function() {
		daoLogradouro.deletar(logradouro);
 	}
	
	this.listarAll = function() {
		daoLogradouro.listarAll();	
	}
	
}