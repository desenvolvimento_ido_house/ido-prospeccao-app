function BusinessDelegateServico() {
 	
	var servico = new Servico();
	var daoServico = new DAOServico();
   
	this.getServico = function() {
		return servico;
	}
	
 	this.iniciar = function() {
 		
 		if (AppInfo.dropAllTableAoIniciar) {
 			daoServico.dropTable();
 		}
 		
	    db.database.transaction(this.criarTabela, function(err) {
	    	console.log("Error processing SQL: " + err.code);
	    }, function() {
	    	console.log("success create table " + servicoInfo.NOME_TABELA);
	    });	
	}
	
	this.criarTabela = function(tx) {
		daoServico.createTable(tx);
	}
	
	this.salvar = function() {  
	 	
		if ( servico.getServico() == null || servico.getServico() == '') {
			alert('Descrição do serviço é obrigatório');//TODO CALLBACK PARA RETORNO COM UM OBJETO
			return false;
		}
		
		if (servico.getDetalhe() == '' || servico.getDetalhe() == null) {
			alert('Detalhe do seriço é obrigatório.');
			return false;
		}
		
		if (servico.getId() != null) {
			daoServico.update(servico);
		} else {
			daoServico.salvar(servico);			
		}
		
		return true;
	}
		
	this.excluir = function() {
		daoServico.deletar(servico);
 	}
	
	this.listarAll = function() {
 		daoServico.listarAll();	
	}
	
	this.getMemoryRows = function() {
		return daoServico.getMemoryRows();
	}
	
}