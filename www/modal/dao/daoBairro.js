var bairroInfo = {
		NOME_TABELA : 'Bairro',
}

function DAOBairro() {
 
	this.salvar = function(bairro) {
		db.database.transaction(
		    function (transaction) {
		    	var sql = 'INSERT INTO ' + bairroInfo.NOME_TABELA + ' (cidade, descricao) VALUES ("' + bairro.getCidade() + '" , "' + bairro.getDescricao() + '" )';
		    	transaction.executeSql(sql);
		    }
		);
	}
	
	this.update = function(bairro) {
		db.database.transaction(
		    function (transaction) {
		    	var sql = 'UPDATE ' + bairroInfo.NOME_TABELA + ' SET cidade = "' + bairro.getCidade() + '" , descricao = "' + bairro.getDescricao() + '" WHERE id = ' + bairro.getId();
		    	transaction.executeSql(sql);
  		    }
	    );
	}
	
	this.deletar = function(bairro) {
		db.database.transaction(
	        function (transaction) {
	            var sql = 'DELETE FROM ' +  bairroInfo.NOME_TABELA + ' WHERE id = ' + bairro.getId();
		    	transaction.executeSql(sql);
		    	bairro.setId(null);
 	        }
	    );
 	}
	
	this.listarAll = function() {
	    db.database.transaction(this.query, this.handleErrors);    
	}
	
 	this.createTable = function(tx) {    
		var sqlCreateTableBairro = "" +
				"	CREATE TABLE IF NOT EXISTS " +  bairroInfo.NOME_TABELA + " (" +
				"		id INTEGER PRIMARY KEY AUTOINCREMENT," +
				"		cidade INTEGER, " +
				"		descricao TEXT " +
				"	)";
		tx.executeSql(sqlCreateTableBairro);
	}
 	
 	this.dropTable = function() {
 		var sql = 'DROP TABLE ' + bairroInfo.NOME_TABELA;
 	    db.database.transaction(
 	        function (transaction) {
 	            transaction.executeSql(sql, [], function() {
 	            	console.log("Drop table " + bairroInfo.NOME_TABELA);
 	            }, this.handleErrors);
 	        }
 	    );
 	}
	
	this.query = function(tx) {
	    tx.executeSql('SELECT * FROM ' +  bairroInfo.NOME_TABELA  + ' order by descricao ',[],function(tx, results) {
	    	bairroInfo.memoryRows = results.rows;    
 	    }, this.handleErrors);
	}
	
	this.sucesso = function(tx, results) {
		console.log("sucesso ao executar o sql.");
	}
	
	this.handleErrors = function(err) {
	    console.log("Error processing SQL: " + err.code);
	}
}