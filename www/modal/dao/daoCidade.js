var cidadeInfo = {
		NOME_TABELA : 'Cidade',
}

function DAOCidade() {
 
	this.salvar = function(cidade) {
		db.database.transaction(
		    function (transaction) {
		    	var sql = 'INSERT INTO ' + cidadeInfo.NOME_TABELA + ' (estado, descricao, cepgeral) VALUES ("' + cidade.getEstado() + '" , "' + cidade.getDescricao() + '" , "' + cidade.getCEPGeral() + '" )';
		    	transaction.executeSql(sql);
		    }
		);
	}
	
	this.update = function(cidade) {
		db.database.transaction(
		    function (transaction) {
		    	var sql = 'UPDATE ' + cidadeInfo.NOME_TABELA + ' SET estado = "' + cidade.getEstado() + '" , descricao = "' + cidade.getDescricao() + '" , cepgeral = "' + cidade.getCEPGeral() + '" WHERE id = ' + cidade.getId();
		    	transaction.executeSql(sql);
  		    }
	    );
	}
	
	this.deletar = function(cidade) {
		db.database.transaction(
	        function (transaction) {
	            var sql = 'DELETE FROM ' +  cidadeInfo.NOME_TABELA + ' WHERE id = ' + cidade.getId();
		    	transaction.executeSql(sql);
		    	cidade.setId(null);
 	        }
	    );
 	}
	
	this.listarAll = function() {
	    db.database.transaction(this.query, this.handleErrors);    
	}
	
 	this.createTable = function(tx) {    
		var sqlCreateTableCidade = "" +
				"	CREATE TABLE IF NOT EXISTS " +  cidadeInfo.NOME_TABELA + " (" +
				"		id INTEGER PRIMARY KEY AUTOINCREMENT," +
				"		estado INTEGER, " +
				"		descricao TEXT, " +
				"		cepgeral TEXT " +
				"	)";
		tx.executeSql(sqlCreateTableCidade);
	}
 	
 	this.dropTable = function() {
 		var sql = 'DROP TABLE ' + cidadeInfo.NOME_TABELA;
 	    db.database.transaction(
 	        function (transaction) {
 	            transaction.executeSql(sql, [], function() {
 	            	console.log("Drop table " + cidadeInfo.NOME_TABELA);
 	            }, this.handleErrors);
 	        }
 	    );
 	}
	
	this.query = function(tx) {
	    tx.executeSql('SELECT * FROM ' +  cidadeInfo.NOME_TABELA  + ' order by descricao ',[],function(tx, results) {
	    	cidadeInfo.memoryRows = results.rows;    
 	    }, this.handleErrors);
	}
	
	this.sucesso = function(tx, results) {
		console.log("sucesso ao executar o sql.");
	}
	
	this.handleErrors = function(err) {
	    console.log("Error processing SQL: " + err.code);
	}
}