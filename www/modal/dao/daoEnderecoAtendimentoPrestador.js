var enderecoAtendimentoPrestadorInfo = {
 		NOME_TABELA : 'EnderecoAtendimentoPrestador'
}

function DAOEnderecoAtendimentoPrestado() {

	this.createTable = function(tx) {    
		var sqlCreateTable = 
			" CREATE TABLE IF NOT EXISTS " +  enderecoAtendimentoPrestadorInfo.NOME_TABELA +  " (" +
			"		id INTEGER PRIMARY KEY AUTOINCREMENT, " +
			"		prestador INTEGER, " +
			"		cep TEXT , " +
			"		estado TEXT, " +
			"		municipio TEXT " +
			"	)";
		tx.executeSql(sqlCreateTable);
	}
	
	this.dropTable = function() {
 		var sql = 'DROP TABLE ' + enderecoAtendimentoPrestadorInfo.NOME_TABELA;
 	    db.database.transaction(
 	        function (transaction) {
 	            transaction.executeSql(sql, [], function() {
 	            	console.log("drop table " + enderecoAtendimentoPrestadorInfo.NOME_TABELA);
 	            }, this.handleErrors);
 	        }
 	    );
 	}
	
	this.salvar = function(enderecoAtendimentoPrestador) {
		db.database.transaction(
		    function (transaction) {
		    	var sql = 'INSERT INTO ' + enderecoAtendimentoPrestadorInfo.NOME_TABELA + ' (prestador, cep, estado, municipio ) VALUES ("' +
		    		enderecoAtendimentoPrestador.getPrestador().getId() + '" , "' + 
		    		enderecoAtendimentoPrestador.getCep()               + '" , "' + 
		    		enderecoAtendimentoPrestador.getEstado()            + '" , "' + 
		    		enderecoAtendimentoPrestador.getMunicipio()         + ' " )';
		    	transaction.executeSql(sql);
		    	
		    	transaction.executeSql('SELECT MAX(ID) AS maxId  FROM ' + enderecoAtendimentoPrestadorInfo.NOME_TABELA + ' order by id ',[],function(tx, results) {
		    		enderecoAtendimentoPrestador.setId(results.rows[0].maxId);
		  	    });
		    }
		);
	}
	
	this.buscar = function(prestador) {
		db.database.transaction(
	        function (transaction) {
	        	transaction.executeSql('SELECT * FROM ' +  enderecoAtendimentoPrestadorInfo.NOME_TABELA  + ' WHERE prestador = ' + prestador.getId(),[],function(tx, results) {
 	    	    	var resultados = results.rows;
 	    	    	if (resultados.length > 0) {
 	    	    		for (var i = 0; i < resultados.length; i++) {
 	    	    			var resultado = resultados[i];
	 	  	    	    	var enderecoAtendimentoPrestador = new EnderecoAtendimentoPrestador();
	 	  	    	    	enderecoAtendimentoPrestador.setId(resultado.id);
	 	  	    	    	enderecoAtendimentoPrestador.setCep(resultado.cep);
	 	  	    	    	enderecoAtendimentoPrestador.setEstado(resultado.estado);
	 	  	    	    	enderecoAtendimentoPrestador.setMunicipio(resultado.municipio);
	 	  	    	    	enderecoAtendimentoPrestador.setPrestador(prestador);
  	 	  	    	    	prestador.getEnderecosAtendimento().push(enderecoAtendimentoPrestador); 	
 	    	    		}
 	    	    	}
    	    	
 	     	    }, this.handleErrors);
	        }
		);
	}
	
	this.deletarAll = function(enderecoAtendimentoPrestador) {
		db.database.transaction(
	        function (transaction) {
	            var sql = 'DELETE FROM ' +  enderecoAtendimentoPrestadorInfo.NOME_TABELA + ' WHERE prestador  = ' + enderecoAtendimentoPrestador.getPrestador().getId();
		    	transaction.executeSql(sql);
  	        }
	    );
 	}
 
}