var estadoInfo = {
		NOME_TABELA : 'Estado',
}

function DAOEstado() {
 
	this.salvar = function(estado) {
		db.database.transaction(
		    function (transaction) {
		    	var sql = 'INSERT INTO ' + estadoInfo.NOME_TABELA + ' (descricao, uf) VALUES ("' + estado.getDescricao() + '" , "' + estado.getUF() + '" )';
		    	transaction.executeSql(sql);
		    }
		);
	}
	
	this.update = function(estado) {
		db.database.transaction(
		    function (transaction) {
		    	var sql = 'UPDATE ' + estadoInfo.NOME_TABELA + ' SET descricao = "' + estado.getDescricao() + '" , uf = "' + estado.getUF() + '" WHERE id = ' + estado.getId();
		    	transaction.executeSql(sql);
  		    }
	    );
	}
	
	this.deletar = function(estado) {
		db.database.transaction(
	        function (transaction) {
	            var sql = 'DELETE FROM ' +  estadoInfo.NOME_TABELA + ' WHERE id = ' + estado.getId();
		    	transaction.executeSql(sql);
		    	estado.setId(null);
 	        }
	    );
 	}
	
	this.listarAll = function() {
	    db.database.transaction(this.query, this.handleErrors);    
	}
	
 	this.createTable = function(tx) {    
		var sqlCreateTableEstado = "" +
				"	CREATE TABLE IF NOT EXISTS " +  estadoInfo.NOME_TABELA + " (" +
				"		id INTEGER PRIMARY KEY AUTOINCREMENT," +
				"		descricao TEXT, " +
				"		uf TEXT " +
				"	)";
		tx.executeSql(sqlCreateTableEstado);
	}
 	
 	this.dropTable = function() {
 		var sql = 'DROP TABLE ' + estadoInfo.NOME_TABELA;
 	    db.database.transaction(
 	        function (transaction) {
 	            transaction.executeSql(sql, [], function() {
 	            	console.log("Drop table " + estadoInfo.NOME_TABELA);
 	            }, this.handleErrors);
 	        }
 	    );
 	}
	
	this.query = function(tx) {
	    tx.executeSql('SELECT * FROM ' +  estadoInfo.NOME_TABELA  + ' order by descricao ',[],function(tx, results) {
	    	estadoInfo.memoryRows = results.rows;    
 	    }, this.handleErrors);
	}
	
	this.sucesso = function(tx, results) {
		console.log("sucesso ao executar o sql.");
	}
	
	this.handleErrors = function(err) {
	    console.log("Error processing SQL: " + err.code);
	}
}