var logradouroInfo = {
		NOME_TABELA : 'Logradouro'
}

function DAOLogradouro() {
 
	this.salvar = function(logradouro) {
		db.database.transaction(
		    function (transaction) {
		    	var sql = 'INSERT INTO ' + logradouroInfo.NOME_TABELA + ' (tipoLogradouro, bairro, descricao, cep) VALUES ("' + logradouro.getTipoLogradouro() + '" , "' + logradouro.getBairro() + '" , "' + logradouro.getDescricao() + '" , "' + logradouro.getCEP() + '" )';
		    	transaction.executeSql(sql);
		    }
		);
	}
	
	this.update = function(logradouro) {
		db.database.transaction(
		    function (transaction) {
		    	var sql = 'UPDATE ' + logradouroInfo.NOME_TABELA + ' SET tipoLogradouro = "' + logradouro.getTipoLogradouro() + '" , bairro = "' + logradouro.getBairro() + '" , descricao = "' + logradouro.getDescricao() + '" , cep = "' + logradouro.getCEP() + '" WHERE id = ' + logradouro.getId();
		    	transaction.executeSql(sql);
  		    }
	    );
	}
	
	this.deletar = function(logradouro) {
		db.database.transaction(
	        function (transaction) {
	            var sql = 'DELETE FROM ' +  logradouroInfo.NOME_TABELA + ' WHERE id = ' + logradouro.getId();
		    	transaction.executeSql(sql);
		    	logradouro.setId(null);
 	        }
	    );
 	}
	
	this.listarAll = function() {
	    db.database.transaction(this.query, this.handleErrors);    
	}
	
 	this.createTable = function(tx) {    
		var sqlCreateTableLogradouro = "" +
				"	CREATE TABLE IF NOT EXISTS " +  logradouroInfo.NOME_TABELA + " (" +
				"		id INTEGER PRIMARY KEY AUTOINCREMENT," +
				"		tipoLogradouro TEXT, " +
				"		bairro INTEGER " +
				"		descricao TEXT, " +
				"		cep TEXT " +
				"	)";
		tx.executeSql(sqlCreateTableLogradouro);
	}
 	
 	this.dropTable = function() {
 		var sql = 'DROP TABLE ' + logradouroInfo.NOME_TABELA;
 	    db.database.transaction(
 	        function (transaction) {
 	            transaction.executeSql(sql, [], function() {
 	            	console.log("Drop table " + logradouroInfo.NOME_TABELA);
 	            }, this.handleErrors);
 	        }
 	    );
 	}
	
	this.query = function(tx) {
	    tx.executeSql('SELECT * FROM ' +  logradouroInfo.NOME_TABELA  + ' order by descricao ',[],function(tx, results) {
	    	logradouroInfo.memoryRows = results.rows;    
 	    }, this.handleErrors);
	}
	
	this.sucesso = function(tx, results) {
		console.log("sucesso ao executar o sql.");
	}
	
	this.handleErrors = function(err) {
	    console.log("Error processing SQL: " + err.code);
	}
}