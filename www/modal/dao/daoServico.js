var servicoInfo = {
		memoryRows : null,
		NOME_TABELA : 'Servico',
}

function DAOServico() {
 
	this.salvar = function(servico) {
		db.database.transaction(
		    function (transaction) {
		    	var sql = 'INSERT INTO ' + servicoInfo.NOME_TABELA + ' (servico, detalhe) VALUES ("' + servico.getServico() + '" , "' + servico.getDetalhe() + '" )';
		    	transaction.executeSql(sql);
		    }
		);
	}
	
	this.update = function(servico) {
		db.database.transaction(
		    function (transaction) {
		    	var sql = 'UPDATE ' + servicoInfo.NOME_TABELA + ' SET servico = "' + servico.getServico() + '" , detalhe = "' + servico.getDetalhe() + '" WHERE id = ' + servico.getId();
		    	transaction.executeSql(sql);
  		    }
	    );
	}
	
	this.getMemoryRows = function() {
		return servicoInfo.memoryRows;
	}
	
	this.deletar = function(servico) {
		db.database.transaction(
	        function (transaction) {
	            var sql = 'DELETE FROM ' +  servicoInfo.NOME_TABELA + ' WHERE id = ' + servico.getId();
		    	transaction.executeSql(sql);
		    	servico.setId(null);
 	        }
	    );
 	}
	
	this.listarAll = function() {
	    db.database.transaction(this.query, this.handleErrors);    
	}
	
 	this.createTable = function(tx) {    
		var sqlCreateTableServico = "" +
				"	CREATE TABLE IF NOT EXISTS " +  servicoInfo.NOME_TABELA + " (" +
				"		id INTEGER PRIMARY KEY AUTOINCREMENT," +
				"		servico TEXT, " +
				"		detalhe TEXT " +
				"	)";
		tx.executeSql(sqlCreateTableServico);
	}
 	
 	this.dropTable = function() {
 		var sql = 'DROP TABLE ' + servicoInfo.NOME_TABELA;
 	    db.database.transaction(
 	        function (transaction) {
 	            transaction.executeSql(sql, [], function() {
 	            	console.log("Drop table " + servicoInfo.NOME_TABELA);
 	            }, this.handleErrors);
 	        }
 	    );
 	}
	
	this.query = function(tx) {
	    tx.executeSql('SELECT * FROM ' +  servicoInfo.NOME_TABELA  + ' order by servico ',[],function(tx, results) {
	    	servicoInfo.memoryRows = results.rows;    
 	    }, this.handleErrors);
	}
	
	this.sucesso = function(tx, results) {
		console.log("sucesso ao executar o sql.");
	}
	
	this.handleErrors = function(err) {
	    console.log("Error processing SQL: " + err.code);
	}
}