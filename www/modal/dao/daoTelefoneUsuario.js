var telefoneUsuarioInfo = {
		memoryRows : null,
		NOME_TABELA : 'TelefoneUsuario'
}

function DAOTelefoneUsuario() {
	
 	this.createTable = function(tx) {    
		var sqlCreateTable = " " +
				" CREATE TABLE IF NOT EXISTS " +  telefoneUsuarioInfo.NOME_TABELA + " (" +
				"	id INTEGER PRIMARY KEY AUTOINCREMENT, " +
				"	usuario INTEGER, " +
				"	telefone TEXT , " +
				"	tipoTelefone TEXT " +
 				" ) ";
		tx.executeSql(sqlCreateTable);
	}
 	
 	this.dropTable = function() {
 		var sql = 'DROP TABLE ' + telefoneUsuarioInfo.NOME_TABELA;
 	    db.database.transaction(
 	        function (transaction) {
 	            transaction.executeSql(sql, [], function() {
 	            	console.log("drop table " + telefoneUsuarioInfo.NOME_TABELA);
 	            }, this.handleErrors);
 	        }
 	    );
 	}
 	
 	this.salvar = function(telefoneUsuario) {
 		db.database.transaction(
		    function (transaction) {
		    	var sql = 'INSERT INTO ' + telefoneUsuarioInfo.NOME_TABELA + ' (usuario, telefone, tipoTelefone) VALUES (" ' +
		    		telefoneUsuario.getUsuario().getId() + '" , "' +
		    		telefoneUsuario.getTelefone() + '" , "' + 
		    		telefoneUsuario.getTipoTelefone() + ' " )';
		    	transaction.executeSql(sql);
		    	
		    }
 		);
 	}
 	
	this.deletar = function(telefoneUsuario) {
		db.database.transaction(
	        function (transaction) {
	            var sql = 'DELETE FROM ' +  telefoneUsuarioInfo.NOME_TABELA + ' WHERE id = ' + telefoneUsuario.getId();
		    	transaction.executeSql(sql);
		    	telefoneUsuario.setId(null);
 	        }
	    );
 	}
	
	this.deletarAll = function(telefoneUsuario) {
		db.database.transaction(
	        function (transaction) {
	            var sql = 'DELETE FROM ' +  telefoneUsuarioInfo.NOME_TABELA + ' WHERE usuario = ' + telefoneUsuario.getUsuario().getId();
		    	transaction.executeSql(sql);
  	        }
	    );
 	}
	
	this.buscar = function(usuario) {
		db.database.transaction(
	        function (transaction) {
	        	transaction.executeSql('SELECT * FROM ' +  telefoneUsuarioInfo.NOME_TABELA  + ' WHERE usuario = ' + usuario.getId(),[],function(tx, results) {
 	    	    	var resultados = results.rows;
 	    	    	if (resultados.length > 0) {
 	    	    		for (var i = 0; i < resultados.length; i++) {
 	    	    			var resultado = resultados[i];
	 	  	    	    	var telefoneUsuario = new TelefoneUsuario();
	 	  	    	    	telefoneUsuario.setId(resultado.id);
	 	  	    	    	telefoneUsuario.setTelefone(resultado.telefone);
	 	  	    	    	telefoneUsuario.setTipoTelefone(resultado.tipoTelefone);
	 	  	    	    	usuario.getTelefones().push(telefoneUsuario); 	
 	    	    		}
 	    	    	}
    	    	
 	     	    }, this.handleErrors);
	        }
		);
	}
	
	this.handleErrors = function(err) {
	    console.log("Error processing SQL: " + err.code);
	}

}